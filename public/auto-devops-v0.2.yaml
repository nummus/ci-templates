include:
  - 'https://nummus.gitlab.io/ci-templates/definitions/default-definitions.gitlab-ci.yaml'
  - 'https://nummus.gitlab.io/ci-templates/definitions/default-refs-definitions.gitlab-ci.yaml'
  - 'https://nummus.gitlab.io/ci-templates/jobs/job-build-node.gitlab-ci.yaml'
  - 'https://nummus.gitlab.io/ci-templates/jobs/job-build-nodejs.gitlab-ci.yaml'
  - 'https://nummus.gitlab.io/ci-templates/jobs/job-build-image.gitlab-ci.yaml'
  - 'https://nummus.gitlab.io/ci-templates/jobs/job-build-dotnet.gitlab-ci.yaml'

#####################################
#  DEPLOY KUBERNETES JOBS
#####################################

# -----------------------------------
#  TEMPLATES
# -----------------------------------

.configure_kubectl: &configure_kubectl
  - SERVER=${HELM_KUBERNETES_SERVER}
  - if [ -z "$HELM_KUBERNETES_SERVER" ]; then SERVER="https://kubernetes.default.svc.cluster.local"; fi
  - kubectl config set-cluster kubernetes --server="$SERVER" --insecure-skip-tls-verify=true
  - kubectl config set-credentials helm-deployer --token="$HELM_KUBERNETES_TOKEN"
  - kubectl config set-context kubernetes --cluster="kubernetes" --namespace="$KUBERNETES_NAMESPACE" --user="helm-deployer"
  - kubectl config use-context kubernetes
  - echo "Server $SERVER configured"

#####################################
#       CONFIGURA ENVIRONMENT       #
#####################################
.configure_environment: &configure_environment |
  build_staging_url() {
    if [ -z "$SUBDOMAIN_PREFIX_STAGING" ]; then
      IFS='.'
      read -a SUBDOMAIN_PREFIX_STAGING <<< "$SUBDOMAIN_PREFIX"
      SUBDOMAIN_PREFIX_STAGING[0]=${SUBDOMAIN_PREFIX_STAGING[0]}-${1}
      IFS=' '
      SUBDOMAIN_PREFIX_STAGING="${SUBDOMAIN_PREFIX_STAGING[*]}"
      export SUBDOMAIN_PREFIX_STAGING="${SUBDOMAIN_PREFIX_STAGING// /.}"
    fi
  }

  configure_environment() {
    echo "Configurando variáveis de ambiente"
  
    mkdir -p tmp/generated/configs
  
    export URL=${SUBDOMAIN_PREFIX}.${KUBERNETES_DOMAIN}

    IFS='-'
    read -a CI_COMMIT_REF_SLUG_PREFIX <<< "$CI_COMMIT_REF_SLUG"
    IFS=' '
  
  
  
    if [ -f "${KUBERNETES_CONFIG_PATH}/${STAGE}-${CI_COMMIT_REF_SLUG}.yaml" ]; then
      build_staging_url $CI_COMMIT_REF_SLUG
      export STAGING_URL=${SUBDOMAIN_PREFIX_STAGING}.${KUBERNETES_DOMAIN}
      export URL=$STAGING_URL
      export DEPLOY=$APPNAME-$CI_COMMIT_REF_SLUG
      export AMBIENTE=$CI_COMMIT_REF_SLUG
      envsubst <${KUBERNETES_CONFIG_PATH}/${STAGE}-${CI_COMMIT_REF_SLUG}.yaml > tmp/generated/configs/${STAGE}.yaml
    elif [ -f "${KUBERNETES_CONFIG_PATH}/${STAGE}-${CI_COMMIT_REF_SLUG_PREFIX}.yaml" ]; then
      build_staging_url $CI_COMMIT_REF_SLUG_PREFIX
      export STAGING_URL=${SUBDOMAIN_PREFIX_STAGING}.${KUBERNETES_DOMAIN}
      export URL=$STAGING_URL
      export DEPLOY=$APPNAME-$CI_COMMIT_REF_SLUG_PREFIX
      export AMBIENTE=$CI_COMMIT_REF_SLUG_PREFIX
      envsubst <${KUBERNETES_CONFIG_PATH}/${STAGE}-${CI_COMMIT_REF_SLUG_PREFIX}.yaml > tmp/generated/configs/${STAGE}.yaml
    else
      export DEPLOY=$APPNAME
      export AMBIENTE=stable
      envsubst <${KUBERNETES_CONFIG_PATH}/${STAGE}.yaml > tmp/generated/configs/${STAGE}.yaml
    fi
  
    echo "Ambiente $AMBIENTE"

  }

.deploy-kubernetes: &deploy-kubernetes-template
  image: $HELM_DEPLOY_IMAGE
  when: manual
  allow_failure: true
  except:
    refs:
      - schedules
    variables:
      - $KUBERNETES_ENABLED == "false"
  before_script:
    - *configure_kubectl
    - *configure_environment
  script: |
    if [ -z "$APPNAME" ]; then
      echo "A variavel APPNAME nao foi definida"
    	exit 1
    fi
    if [ -z "$ENABLE_NUMMUS_REPOS" ]; then
      ENABLE_NUMMUS_REPOS="false"
    fi	
    if [ "$ENABLE_NUMMUS_REPOS" = "true" ]; then
      helm repo add nummus $NUMMUS_CHARTS_HOST
    fi
    
    echo "Publicando no namespace $KUBERNETES_NAMESPACE"
    
    configure_environment
    
    if [ -z "$CHART_VERSION" ]; then
        echo "A versão do chart não foi definida na variável CHART_VERSION"
        exit 1
    fi
    
    CHART_INSTALED=$(helm list -n $KUBERNETES_NAMESPACE -q -f "^${DEPLOY}$" | wc -l)
    
    if [ ${CHART_INSTALED} -eq 0 ]
    then
        echo "Instalando deploy $DEPLOY"
        helm install $DEPLOY --set appVersion=$CI_COMMIT_REF_SLUG --set labels.app=$APPNAME -f "tmp/generated/configs/${STAGE}.yaml" --namespace $KUBERNETES_NAMESPACE $HELM_CHART --version $CHART_VERSION --atomic --wait --timeout $HELM_TIMEOUT
        exit 0
    fi
    
    echo "Atualizando deploy $DEPLOY"
    helm upgrade --history-max 0 $DEPLOY --set appVersion=$CI_COMMIT_REF_SLUG --set labels.app=$APPNAME -f "tmp/generated/configs/${STAGE}.yaml" --namespace $KUBERNETES_NAMESPACE $HELM_CHART --version $CHART_VERSION --atomic --wait --timeout $HELM_TIMEOUT
  artifacts:
    paths:
      - /tmp/generated/configs

.stop-kubernetes: &stop-kubernetes-template
  image: $HELM_DEPLOY_IMAGE
  when: manual
  allow_failure: true
  except:
    refs:
      - schedules
    variables:
      - $KUBERNETES_ENABLED == "false"
  before_script:
    - *configure_kubectl
    - *configure_environment
  script: |
    configure_environment
    
    echo "Removendo deploy o $DEPLOY"
    helm-deployer uninstall $DEPLOY --namespace $KUBERNETES_NAMESPACE 

.stop-old-replicas-kubernetes: &stop-old-replicas-kubernetes-template
  image: $HELM_DEPLOY_IMAGE
  when: manual
  allow_failure: true
  except:
    refs:
      - schedules
    variables:
      - $KUBERNETES_ENABLED == "false"
      - $AUTO_PROMOTE_TO_STABLE == "true"
  before_script:
    - *configure_kubectl
    - *configure_environment
  script: |
    bash
    if [ -z "$APPNAME" ]; then
        echo "A variavel APPNAME nao foi definida"
    	exit 1
    fi
    
    configure_environment
    
    for deploy in $(helm ls -n $KUBERNETES_NAMESPACE -qaf "^$APPNAME-+(release|master|bugfix|hotfix|feature|sprint|[0-9])" | grep -v "^$DEPLOY$" | awk {'print $1'}); do
    
        echo "Removendo deploy o $deploy"
    
        helm-deployer uninstall ${deploy} --namespace $KUBERNETES_NAMESPACE
    
    done;

.rollback-kubernetes: &rollback-kubernetes-template
  image: $HELM_DEPLOY_IMAGE
  when: manual
  allow_failure: true
  except:
    refs:
      - schedules
    variables:
      - $KUBERNETES_ENABLED == "false"
      - $AUTO_PROMOTE_TO_STABLE == "false"
  before_script:
    - *configure_kubectl
    - *configure_environment
  script: |
    configure_environment
    
    echo "Removendo deploy o $DEPLOY"
    
    helm-deployer rollback $DEPLOY --namespace $KUBERNETES_NAMESPACE --wait --timeout $HELM_KUBERNETES_TIMEOUT

.promote-to-stable-kubernetes: &promote-to-stable-kubernetes-template
  image: $HELM_DEPLOY_IMAGE
  when: manual
  allow_failure: true
  except:
    refs:
      - schedules
    variables:
      - $KUBERNETES_ENABLED == "false"
      - $AUTO_PROMOTE_TO_STABLE == "true"
  before_script:
    - *configure_kubectl
  script: |
    echo "$KUBERNETES_NAMESPACE"
    echo "Substituindo variáveis"
    mkdir -p /tmp/generated/configs
    envsubst <${CI_PROJECT_DIR}/configs/routes.yaml > /tmp/generated/configs/routes.yaml
    echo "Promovendo rota"
    cat /tmp/generated/configs/routes.yaml
    kubectl apply -f /tmp/generated/configs/routes.yaml

# -----------------------------------
#  JOBS KUBERNETES TEST
# -----------------------------------

deploy-kubernetes-test:
  <<: *deploy-kubernetes-template
  extends:
    - .default-refs
  stage: test
  variables:
    ENV: 'test'
    STAGE: 'test'
    KUBERNETES_DOMAIN: $KUBERNETES_DOMAIN_TEST
    KUBERNETES_NAMESPACE: $HELM_KUBERNETES_NAMESPACE_TEST
    ANALYTICS_ID: $ANALYTICS_ID_TEST
    URL_API_DOCUMENTATION: $URL_API_DOCUMENTATION_TEST
  only:
    variables:
      - $TEST_ENVIRONMENT_ENABLED == "true"
  environment:
    name: test/k8s/$CI_COMMIT_REF_NAME
    url: "https://${APPNAME}-${CI_COMMIT_REF_SLUG}.${KUBERNETES_DOMAIN_TEST}"
    on_stop: "stop-kubernetes-test"
  tags:
    - nummus

stop-kubernetes-test:
  <<: *stop-kubernetes-template
  stage: test
  extends:
    - .default-refs
  variables:
    ENV: 'test'
    KUBERNETES_NAMESPACE: $HELM_KUBERNETES_NAMESPACE_TEST
    KUBERNETES_DOMAIN: $KUBERNETES_DOMAIN_TEST
    ANALYTICS_ID: $ANALYTICS_ID_TEST
    URL_API_DOCUMENTATION: $URL_API_DOCUMENTATION_TEST
  only:
    variables:
      - $TEST_ENVIRONMENT_ENABLED == "true"
  environment:
    name: test/k8s/$CI_COMMIT_REF_NAME
    action: stop
  tags:
    - nummus

stop-old-replicas-kubernetes-test:
  <<: *stop-old-replicas-kubernetes-template
  stage: post test
  extends:
    - .default-refs
  variables:
    ENV: 'test'
    STAGE: 'test'
    KUBERNETES_NAMESPACE: $HELM_KUBERNETES_NAMESPACE_TEST
    KUBERNETES_DOMAIN: $KUBERNETES_DOMAIN_TEST
    ANALYTICS_ID: $ANALYTICS_ID_TEST
    URL_API_DOCUMENTATION: $URL_API_DOCUMENTATION_TEST
  only:
    variables:
      - $TEST_ENVIRONMENT_ENABLED == "true"
  environment:
    name: test/k8s/$CI_COMMIT_REF_NAME
    action: stop
  tags:
    - nummus

promote-to-stable-kubernetes-test:
  <<: *promote-to-stable-kubernetes-template
  extends:
    - .default-refs
  variables:
    DOMAIN: $KUBERNETES_DOMAIN_TEST
    URL: $KUBERNETES_URL_TEST
    KUBERNETES_NAMESPACE: $HELM_KUBERNETES_NAMESPACE_TEST
    ANALYTICS_ID: $ANALYTICS_ID_TEST
    URL_API_DOCUMENTATION: $URL_API_DOCUMENTATION_TEST
  stage: post test
  only:
    variables:
      - $TEST_ENVIRONMENT_ENABLED == "true"
  tags:
    - nummus

rollback-kubernetes-test:
  <<: *rollback-kubernetes-template
  stage: post test
  variables:
    STAGE: 'test'
    DOMAIN: $KUBERNETES_DOMAIN_TEST
    KUBERNETES_NAMESPACE: $HELM_KUBERNETES_NAMESPACE_TEST
    ANALYTICS_ID: $ANALYTICS_ID_TEST
    URL_API_DOCUMENTATION: $URL_API_DOCUMENTATION_TEST
  extends:
    - .default-refs
  only:
    variables:
      - $TEST_ENVIRONMENT_ENABLED == "true"
  environment:
    name: test/k8s/$CI_COMMIT_REF_NAME
    action: stop
  tags:
    - nummus

# -----------------------------------
#  JOBS KUBERNETES PRODUCTION
# -----------------------------------

deploy-kubernetes-production:
  <<: *deploy-kubernetes-template
  stage: production
  extends:
    - .refs-production
  variables:
    ENV: 'production'
    STAGE: 'production'
    KUBERNETES_DOMAIN: $KUBERNETES_DOMAIN_PRODUCTION
    KUBERNETES_NAMESPACE: $HELM_KUBERNETES_NAMESPACE_PRODUCTION
    ANALYTICS_ID: $ANALYTICS_ID_PRODUCTION
    URL_API_DOCUMENTATION: $URL_API_DOCUMENTATION_PRODUCTION
  only:
    variables:
      - $PRODUCTION_ENVIRONMENT_ENABLED == "true"
  environment:
    name: production/k8s/$CI_COMMIT_REF_NAME
    url: "https://${APPNAME}-${CI_COMMIT_REF_SLUG}.${KUBERNETES_DOMAIN_PRODUCTION}"
    on_stop: "stop-kubernetes-production"
  tags:
    - nummus

stop-kubernetes-production:
  <<: *stop-kubernetes-template
  extends:
    - .refs-production
  stage: production
  variables:
    ENV: 'production'
    STAGE: 'production'
    KUBERNETES_NAMESPACE: $HELM_KUBERNETES_NAMESPACE_PRODUCTION
    KUBERNETES_DOMAIN: $KUBERNETES_DOMAIN_PRODUCTION
    ANALYTICS_ID: $ANALYTICS_ID_PRODUCTION
    URL_API_DOCUMENTATION: $URL_API_DOCUMENTATION_PRODUCTION
  only:
    variables:
      - $PRODUCTION_ENVIRONMENT_ENABLED == "true"
  environment:
    name: production/k8s/$CI_COMMIT_REF_NAME
    action: stop
  tags:
    - nummus

stop-old-replicas-kubernetes-production:
  <<: *stop-old-replicas-kubernetes-template
  extends:
    - .refs-production
  stage: post production
  variables:
    ENV: 'production'
    STAGE: 'production'
    KUBERNETES_NAMESPACE: $HELM_KUBERNETES_NAMESPACE_PRODUCTION
    KUBERNETES_DOMAIN: $KUBERNETES_DOMAIN_PRODUCTION
    ANALYTICS_ID: $ANALYTICS_ID_PRODUCTION
    URL_API_DOCUMENTATION: $URL_API_DOCUMENTATION_PRODUCTION
  only:
    variables:
      - $PRODUCTION_ENVIRONMENT_ENABLED == "true"
  environment:
    name: production/k8s/$CI_COMMIT_REF_NAME
    action: stop
  tags:
    - nummus

promote-to-stable-kubernetes-production:
  <<: *promote-to-stable-kubernetes-template
  extends:
    - .refs-production
  variables:
    DOMAIN: $KUBERNETES_DOMAIN_PRODUCTION
    URL: $KUBERNETES_URL_PRODUCTION
    KUBERNETES_NAMESPACE: $HELM_KUBERNETES_NAMESPACE_PRODUCTION
    ANALYTICS_ID: $ANALYTICS_ID_PRODUCTION
    URL_API_DOCUMENTATION: $URL_API_DOCUMENTATION_PRODUCTION
  stage: post production
  only:
    variables:
      - $PRODUCTION_ENVIRONMENT_ENABLED == "true"
  tags:
    - nummus

rollback-kubernetes-production:
  <<: *rollback-kubernetes-template
  stage: post production
  variables:
    DOMAIN: $KUBERNETES_DOMAIN_PRODUCTION
    KUBERNETES_NAMESPACE: $HELM_KUBERNETES_NAMESPACE_PRODUCTION
    ANALYTICS_ID: $ANALYTICS_ID_PRODUCTION
    URL_API_DOCUMENTATION: $URL_API_DOCUMENTATION_PRODUCTION
  extends:
    - .refs-production
  only:
    variables:
      - $PRODUCTION_ENVIRONMENT_ENABLED == "true"
  environment:
    name: production/k8s/$CI_COMMIT_REF_NAME
    action: stop
  tags:
    - nummus

#####################################
#  MIGRATION JOBS
#####################################

.migrate-database: &migrate-database-template
  image: $NODEJS_IMAGE
  when: on_success
  except:
    refs:
      - schedules
    variables:
      - $MIGRATE_DATABASE_ENABLED == "false"
  script:
    - npm install
    - npm run migrate
  cache:
    key: node
    paths:
      - node_modules/

migrate-database-test:
  <<: *migrate-database-template
  extends:
    - .default-refs
  allow_failure: true
  variables:
    ENV: "test"
    DB_HOST: "$TEST_DB_HOST"
    DB_PORT: "$TEST_DB_PORT"
    DB_USER: "$TEST_DB_USER"
    DB_PASSWORD: "$TEST_DB_PASSWORD"
    DB_DATABASE: "$TEST_DB_DATABASE"
  stage: pre test
  only:
    variables:
      - $TEST_ENVIRONMENT_ENABLED == "true"
  tags:
    - nummus

migrate-database-prodution:
  <<: *migrate-database-template
  when: manual
  extends:
    - .refs-production
  allow_failure: false
  variables:
    ENV: "production"
    DB_HOST: "$PROD_DB_HOST"
    DB_PORT: "$PROD_DB_PORT"
    DB_USER: "$PROD_DB_USER"
    DB_PASSWORD: "$PROD_DB_PASSWORD"
    DB_DATABASE: "$PROD_DB_DATABASE"
  stage: pre production
  only:
    variables:
      - $PRODUCTION_ENVIRONMENT_ENABLED == "true"
  tags:
    - nummus

